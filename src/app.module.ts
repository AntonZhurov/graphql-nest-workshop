import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import {
  ColumnQueryResolver,
  ColumnFieldResolver,
  CardFieldResolver,
  CardQueryResolver,
} from './resolvers';

@Module({
  imports: [
    GraphQLModule.forRoot({
      playground: true,
    }),
  ],
  controllers: [],
  providers: [
    ColumnQueryResolver,
    ColumnFieldResolver,
    CardFieldResolver,
    CardQueryResolver,
  ],
})
export class AppModule {}
