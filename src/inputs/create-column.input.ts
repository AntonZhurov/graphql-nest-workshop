import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateColumnInput {
  @Field()
  title: string;
}
