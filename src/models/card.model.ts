import { ColumnModel } from './column.model';

export class CardModel {
  id: number;

  title: string;

  columnId: number;

  column?: ColumnModel;
}
