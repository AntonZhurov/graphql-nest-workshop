import { CardModel } from './card.model';
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType('Column')
export class ColumnModel {
  @Field(() => Int, { nullable: false })
  id: number;

  title: string;

  @Field(() => [CardModel], { nullable: false })
  cards?: CardModel[];
}
