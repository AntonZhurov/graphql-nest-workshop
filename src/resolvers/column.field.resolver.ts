import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CardModel, ColumnModel } from '../models';
import { data } from '../mock';

@Resolver(() => ColumnModel)
export class ColumnFieldResolver {
  @ResolveField(() => CardModel, { name: 'cards', nullable: false })
  cards(@Parent() column: ColumnModel): CardModel[] {
    return data.cards.filter((card) => card.columnId === column.id);
  }
}
