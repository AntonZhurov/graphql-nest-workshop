import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ColumnModel } from '../models';
import { data as mockData } from '../mock';
import { CreateColumnInput } from '../inputs';
import { getRandomId } from '../utils';

@Resolver()
export class ColumnQueryResolver {
  @Query(() => [ColumnModel], { nullable: false })
  columns(): ColumnModel[] {
    return mockData.columns;
  }

  @Mutation(() => ColumnModel, { nullable: false })
  createColumn(@Args() data: CreateColumnInput) {
    const column: ColumnModel = {
      id: getRandomId(),
      title: data.title,
    };

    mockData.columns = [...mockData.columns, column];

    return column;
  }
}
